#ifndef NOTEPAD_HPP
#define NOTEPAD_HPP

#include <QMainWindow>
#include <QTextEdit>
#include <QLabel>
#include <QTreeView>

#include "highlighter.hpp"
#include "fileviewer.hpp"
#include "filemanager.hpp"
#include "menu.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class Notepad; }
QT_END_NAMESPACE

class Notepad : public QMainWindow
{
  Q_OBJECT

public:
  explicit Notepad(QWidget *parent = nullptr);
  ~Notepad();

private slots:
  void newDocument();

  void open();

  void save();

  void saveAs();

  void print();

  void exit();

  void copy();

  void cut();

  void paste();

  void undo();

  void redo();

  void selectFont();

  void setFontBold(bool bold);

  void setFontUnderline(bool underline);

  void setFontItalic(bool italic);

  void about();

  void cursorPositionShow();

  void textChanged();

  void select_bigger();

  void openWithPath(QString fileName);

  void openByIndex(const QModelIndex &index);

private:
  Ui::Notepad *ui;
  Highlighter *highlighter;
  QTextEdit* textEdit;
  QLabel* label;
  QTreeView* treeView;
  FileViewer *fileViewer;
  FileManager *fileManager;
  Menu *menu;
  QString currentFile;
};
#endif // NOTEPAD_HPP
