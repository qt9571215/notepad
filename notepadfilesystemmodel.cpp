#include "notepadfilesystemmodel.hpp"

NotepadFileSystemModel::NotepadFileSystemModel(QObject *parent)
  : QFileSystemModel{parent}
{

}

QVariant NotepadFileSystemModel::data(const QModelIndex &index, int role) const
{
  if(role == Qt::DisplayRole || role == Qt::DecorationRole)
    {
      return QFileSystemModel::data(index, role);
    }
  return QVariant();
}
