#ifndef FILEVIEWER_HPP
#define FILEVIEWER_HPP

#include <QFile>
#include <QFileSystemModel>

class FileViewer
{
public:
  FileViewer(QString filepath);

  QFileSystemModel* getModel();

  static QString getQtCreatorProjectsLocation();

  void onDoubleClicked(const QModelIndex &index);

private:
  QString filepath;
  QFileSystemModel *model;
};

#endif // FILEVIEWER_HPP
