#ifndef FILEMANAGER_HPP
#define FILEMANAGER_HPP

#include <QWidget>

class FileManager : public QWidget
{
  Q_OBJECT
public:
  FileManager();

  QString getFileName();

  QString getFileContent();

  void setFileContent(QString fileContent);

  void setFileName(QString filepath);

public slots:

  void create();

  void open(QString filepath);

  void save();

  void print();

private:
  QString mFileName;
  QString mFileContent;
  QString mFilePath;
};

#endif // FILEMANAGER_HPP
