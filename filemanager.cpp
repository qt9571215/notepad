#include "filemanager.hpp"

#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QException>
#include <QDebug>

FileManager::FileManager() : mFileName(), mFileContent()
{
}

QString FileManager::getFileName()
{
  return mFileName;
}

QString FileManager::getFileContent()
{
  return mFileContent;
}

void FileManager::setFileContent(QString fileContent)
{
  mFileContent = fileContent;
}

void FileManager::setFileName(QString filepath)
{
  auto index = filepath.lastIndexOf('/');
  if(index == -1)
    {
      mFileName = filepath;
    }
  else
    {
      mFileName = filepath.right(filepath.size() - index - 1);
      mFilePath = filepath.left(index + 1);
    }
}

void FileManager::create()
{
  mFileName.clear();
  mFileContent.clear();
}

void FileManager::open(QString filepath)
{
  QFile file(filepath);
  qint32 index = filepath.lastIndexOf('/');
  mFileName = filepath.right(filepath.size() - index - 1);
  mFilePath = filepath.left(index + 1);
  qDebug() << "In open: " << mFilePath;
  if(!file.open(QIODevice::ReadOnly | QFile::Text))
    {
      throw QFileDevice::OpenError;
    }
  QTextStream in(&file);
  mFileContent = in.readAll();
  file.close();
}

void FileManager::save()
{
  QFile file(mFilePath + mFileName);
  qDebug() << "In save: " << mFilePath + mFileName;
  if(!file.open(QIODevice::WriteOnly | QFile::Text)) //  QIODevice::Append
    {
      throw QFileDevice::OpenError;
    }
  QTextStream out(&file);
  qDebug() << "Out: " << mFileContent;
  out << mFileContent;
  file.close();
}

void FileManager::print()
{

}
