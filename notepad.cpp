#include "notepad.hpp"
#include "./ui_notepad.h"
#include "fileviewer.hpp"

#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#if defined(QT_PRINTSUPPORT_LIB)
#include <QtPrintSupport/qtprintsupportglobal.h>
#if QT_CONFIG(printer)
#if QT_CONFIG(printdialog)
#include <QPrintDialog>
#endif // QT_CONFIG(printdialog)
#include <QPrinter>
#endif // QT_CONFIG(printer)
#endif // QT_PRINTSUPPORT_LIB
#include <QFont>
#include <QFontDialog>
#include <QShortcut>
#include <QDebug>
#include <QLabel>

Notepad::Notepad(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::Notepad)
{
  ui->setupUi(this);
  treeView    = new QTreeView(this);
  menu        = new Menu(this);
  textEdit    = new QTextEdit(this);
  label       = new QLabel(this);
  highlighter = new Highlighter(textEdit->document());
  setMenuBar(menu);
  QShortcut* select_bigger = new QShortcut(QKeySequence("Ctrl+W"), textEdit);
  ui->horizontalLayout->setAlignment(Qt::AlignLeft);
  ui->horizontalLayout->addWidget(treeView);
  //treeView
  treeView->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
  ui->text->addWidget(textEdit);
  label->setText("0, 0");
  label->sizePolicy().setVerticalPolicy(QSizePolicy::Maximum);
  label->setAlignment(Qt::AlignRight);
  ui->text->addWidget(label);
  QString path = QFileDialog::getOpenFileName(textEdit,
                                           tr("Open File"),
                                           FileViewer::getQtCreatorProjectsLocation(),
                                           tr("All Files (*)"));

  qDebug() << "Path:" << path;
  qint32 index = path.lastIndexOf('/');
  QString rootPath = path.left(index);

  fileViewer = new FileViewer(rootPath);
  treeView->setModel(fileViewer->getModel());
  treeView->setColumnHidden(1, true);
  treeView->setColumnHidden(2, true);
  treeView->setColumnHidden(3, true);


  fileManager = new FileManager();

  treeView->setRootIndex(fileViewer->getModel()->index(rootPath));
  fileViewer->getModel()->setRootPath(rootPath);

  connect(menu->fileMenu->newAction, &QAction::triggered, this, &Notepad::newDocument);
  connect(menu->fileMenu->openAction, &QAction::triggered, this, &Notepad::open);
  connect(menu->fileMenu->saveAction, &QAction::triggered, this, &Notepad::save);
  connect(menu->fileMenu->saveAsAction, &QAction::triggered, this, &Notepad::saveAs);
  connect(menu->fileMenu->printAction, &QAction::triggered, this, &Notepad::print);
  connect(menu->fileMenu->exitAction, &QAction::triggered, this, &Notepad::exit);
  connect(menu->editMenu->copyAction, &QAction::triggered, this, &Notepad::copy);
  connect(menu->editMenu->pasteAction, &QAction::triggered, this, &Notepad::paste);
  connect(menu->editMenu->cutAction, &QAction::triggered, this, &Notepad::cut);
  connect(menu->editMenu->undoAction, &QAction::triggered, this, &Notepad::undo);
  connect(menu->editMenu->redoAction, &QAction::triggered, this, &Notepad::redo);
  connect(menu->editMenu->selectFontAction, &QAction::triggered, this, &Notepad::selectFont);
  connect(menu->editMenu->boldAction, &QAction::toggled, this, &Notepad::setFontBold);
  connect(menu->editMenu->underlineAction, &QAction::toggled, this, &Notepad::setFontUnderline);
  connect(menu->editMenu->italicAction, &QAction::toggled, this, &Notepad::setFontItalic);
  //connect(ui->actionAbout, &QAction::triggered, this, &Notepad::about);
  connect(textEdit, &QTextEdit::cursorPositionChanged, this, &Notepad::cursorPositionShow);
  connect(textEdit, &QTextEdit::textChanged, this, &Notepad::textChanged);
  connect(select_bigger, &QShortcut::activated, this, &Notepad::select_bigger);

  connect(treeView, &QTreeView::doubleClicked, this, &Notepad::openByIndex);
  qDebug() << path;
  openWithPath(path);


  // Disable menu actions for unavailable features
   #if !defined(QT_PRINTSUPPORT_LIB) || !QT_CONFIG(printer)
       ui->actionPrint->setEnabled(false);
   #endif

   #if !QT_CONFIG(clipboard)
       ui->actionCut->setEnabled(false);
       ui->actionCopy->setEnabled(false);
       ui->actionPaste->setEnabled(false);
   #endif

}

Notepad::~Notepad()
{
  delete ui;
}

void Notepad::newDocument()
{
  fileManager->create();
  textEdit->setText(QString());
}

void Notepad::open()
{
  QString fileName = QFileDialog::getOpenFileName(this, "Open the file");
  try {
    fileManager->open(fileName);
  } catch (QFileDevice::FileError &caught) {
    QMessageBox::warning(this, "Warning", "Cannot open file");
    return;
  }
  textEdit->setText(fileManager->getFileContent());
  setWindowTitle(fileName);
}

void Notepad::openWithPath(QString filePath)
{
  try {
    fileManager->open(filePath);
  } catch (QFileDevice::FileError &caught) {
    QMessageBox::warning(this, "Warning", "Cannot open file");
    return;
  }
  textEdit->setText(fileManager->getFileContent());
  setWindowTitle(fileManager->getFileName());
}

void Notepad::openByIndex(const QModelIndex &index)
{
  QString filePath = fileViewer->getModel()->filePath(index);
  //qDebug() << "Filename: "<< filePath;
  openWithPath(filePath);
}

void Notepad::save()
{
  QString fileName = fileManager->getFileName();
  if(fileName.isEmpty())
    {
      fileManager->setFileName(QFileDialog::getSaveFileName(this, "Save"));
      qDebug() << "Filename: "<< fileName;
    }
  try {
    fileManager->setFileContent(textEdit->toPlainText());
    qDebug() << "Filecontent: "<< fileManager->getFileContent();
    fileManager->save();
  } catch (QFileDevice::FileError &caught) {
    QMessageBox::warning(this, "Warning", "Cannot find file");
    return;
  }
  setWindowTitle(fileName);
  //emit fileViewer->getModel()->;
}

void Notepad::saveAs()
{
  fileManager->setFileName(QFileDialog::getSaveFileName(this, "Save as"));
  try {
    fileManager->setFileContent(textEdit->toPlainText());
    fileManager->save();
  } catch (QFileDevice::FileError &caught) {
    QMessageBox::warning(this, "Warning", "Cannot find file");
    return;
  }
  setWindowTitle(fileManager->getFileName());
}

void Notepad::print()
{
#if defined(QT_PRINTSUPPORT_LIB) && QT_CONFIG(printer)
     QPrinter printDev;
 #if QT_CONFIG(printdialog)
     QPrintDialog dialog(&printDev, this);
     if (dialog.exec() == QDialog::Rejected)
         return;
 #endif // QT_CONFIG(printdialog)
     textEdit->print(&printDev);
 #endif // QT_CONFIG(printer)
}

void Notepad::exit()
{
   QCoreApplication::quit();
}

void Notepad::copy()
{
#if QT_CONFIG(clipboard)
     textEdit->copy();
 #endif
}

void Notepad::cut()
{
#if QT_CONFIG(clipboard)
     textEdit->cut();
 #endif
}

void Notepad::paste()
{
#if QT_CONFIG(clipboard)
     textEdit->paste();
 #endif
}

void Notepad::undo()
{
  textEdit->undo();
}

void Notepad::redo()
{
  textEdit->redo();
}

void Notepad::selectFont()
{
  bool fontSelected;
      QFont font = QFontDialog::getFont(&fontSelected, this);
      if (fontSelected)
          textEdit->setFont(font);
}

void Notepad::setFontBold(bool bold)
{
  if(bold)
    {
      textEdit->setFontWeight(QFont::Bold);
    }
  else
    {
      textEdit->setFontWeight(QFont::Normal);
    }
}

void Notepad::setFontUnderline(bool underline)
{
  textEdit->setFontUnderline(underline);
}

void Notepad::setFontItalic(bool italic)
{
  textEdit->setFontItalic(italic);
}

void Notepad::about()
{
  QMessageBox::information(this, "About", "Notepad is a text editor, i.e., an app specialized in editing plain text. "
                                          "It can edit text files (bearing the \". txt\" filename extension) and "
                                          "compatible formats, such as batch files, INI files, and log files. Notepad offers only the most basic text manipulation functions, such as finding and replacing text.");
}

void Notepad::cursorPositionShow()
{
  auto cursor = textEdit->textCursor();
  label->setText(QString::number(cursor.blockNumber() + 1) + ", " +
                     QString::number(cursor.columnNumber() + 1));
  cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);

}

void Notepad::textChanged()
{

}

void Notepad::select_bigger()
{
  auto cursor = textEdit->textCursor();
  cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
}

