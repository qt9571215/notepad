#include "fileviewer.hpp"

#include <QFileSystemModel>
#include <QFileDialog>
#include <QSettings>
#include <QStandardPaths>
#include <QDebug>
//#include "notepadfilesystemmodel.hpp"

FileViewer::FileViewer(QString filepath) : filepath(filepath)
{
  model = new QFileSystemModel;
  //model->setReadOnly(false);
  qDebug() << "Index: " << filepath;
  model->setRootPath(filepath);
  qDebug() << "Root: " << model->rootPath();
  // Set the root path for the file tree
}

QFileSystemModel *FileViewer::getModel()
{
  return model;
}

void FileViewer::onDoubleClicked(const QModelIndex &index)
{
  QString fileName = model->fileName(index); // Assuming you have a QFileSystemModel named 'model'
}

QString FileViewer::getQtCreatorProjectsLocation()
{
    // Construct the path to the Qt Creator configuration file
    QString configFilePath = QDir::homePath() + "/AppData/Roaming/QtProject/QtCreator.ini";
    qDebug() << "Config: " << configFilePath;

    // Read the projects location setting from the configuration file
    QSettings settings(configFilePath, QSettings::IniFormat);
    QString projectsLocation = settings.value("Directories/Projects").toString();
    qDebug() << "Location: " << projectsLocation;

    return projectsLocation;
}

