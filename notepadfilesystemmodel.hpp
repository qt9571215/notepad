#ifndef NOTEPADFILESYSTEMMODEL_HPP
#define NOTEPADFILESYSTEMMODEL_HPP

#include <QFileSystemModel>
#include <QObject>

class NotepadFileSystemModel : public QFileSystemModel
{
  Q_OBJECT
public:
  explicit NotepadFileSystemModel(QObject *parent = nullptr);

  // QAbstractItemModel interface
public:
  QVariant data(const QModelIndex &index, int role) const;
};

#endif // NOTEPADFILESYSTEMMODEL_HPP
