#ifndef TEXTEDITFIELD_HPP
#define TEXTEDITFIELD_HPP

#include <QObject>
#include <QTextEdit>

class TextEditField : public QTextEdit
{
  Q_OBJECT
public:
  TextEditField(QWidget* parent = nullptr);

  void newDocument();

  void open();

  void save();

  void saveAs();

  void print();

  void exit();

  void copy();

  void cut();

  void paste();

  void undo();

  void redo();

  void selectFont();

  void setFontBold(bool bold);

  void setFontUnderline(bool underline);

  void setFontItalic(bool italic);

  void cursorPositionShow();

  void select_bigger();

  void openWithName(QString fileName);

  void openByIndex(const QModelIndex &index);

};

#endif // TEXTEDITFIELD_HPP
